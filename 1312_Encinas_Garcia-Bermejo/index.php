<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="description" content="Pagina de compra de Películas">
  <meta name="Keywords" content="HTML, CSS">
  <meta name="Authors" content="Javier Encinas y Víctor García-Bermejo">
  <meta http-equiv="refresh" content="30">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <!-- Fuente escogida para la página -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">

  <!-- Nombre de la página -->
  <title> Peliculas La Foca </title>
</head>

<body>
  <!-- Primer menu -->

  <div class="menu">
    <div class="logo">
      <a href="record.html"><img class="logo" src="media/seal.png" alt="Logo foca"></a>
    </div>

      <a href="index.php"><img class="nombre" src="media/foca2.png" alt="LA FOCA"></a>



      <?php
        session_start();
        $usuario = $_SESSION["usuario"];
        if(isset($usuario)){
          print('<div class="login">
            <a href="./record.html">'.$usuario.'</a>
          </div>
          <div class="registro">
            <a href="./destroysession.php">Logout</a>
          </div>');
        }else{
          print('<div class="login">
            <a href="./login.php">Login</a>
          </div>
          <div class="registro">
            <a href="./register.html">Registro</a>
          </div>');
        }
        ?>

  </div>

  <!-- Segundo Menu -->

  <div class="menu2">
    <div class="carrito">
      <a href="#carrito"><img id="carrito" alt="Carrito" src="media/carrito.png"></a>
    </div>
    <div class="buscador">
      <form method="get" action="http://www.google.com/search"><input type="text" placeholder="SEARCH" name="q"></form>
      <div class="lupa"><img src="media/lupa.png" alt="lupa" id="lupa"></div>
    </div>
</div>

    <!-- Estructura -->
    <div class ="estructura">
      <div class="col-2 lateral">
        <ul class="desplegable">
          <li><a href="">Noticias</a></li>
          <li><a href="">Estrenos</a></li>
          <li><a href="">Géneros</a>
          <ul class="generos">
            <li><a href="#Acción" > Acción</a><br></li>
            <li><a href="#Drama" > Drama</a><br></li>
            <li><a href="#Comedia" > Comedia</a><br></li>
            <li><a href="#CienciaFiccion" > Ciencia Ficción</a><br></li>
            <li><a href="#Fantasía" > Fantasía</a><br></li>
            <li><a href="#Terror" > Terror</a><br></li>
            <li><a href="#Romance" > Romance</a><br></li>
            <li><a href="#Musical" > Musical</a><br></li>
            <li><a href="#Suspense" > Suspense</a><br></li>
          </ul>
          </li>
        </ul>
      </div>
      <div class="col-9 contenido">
        <div class="fila uno">
          <div class="col-4 col11">
            <div class="car">
              <a href="film.html"><img class="it" src="media/ItCartel.jpg" alt="Cartel IT"></a>
            </div>
          </div>
          <div class="col-4 col12">
            <div class="car">
              <a href="film.html"><img class="fightclub" src="media/FightClubCartel.jpg" alt="Cartel Fight Club"></a>
            </div>
          </div>
          <div class="col-4 col13">
            <div class="car">
              <a href="film.html"><img class="deadpool" src="media/DeadpoolCartel.jpg" alt="Cartel Deadpool"></a>
            </div>
          </div>
        </div>
        <div class="fila dos">
          <div class="car">
            <div class="col-4 col21">
              <a href="film.html"><img class="DarkKnight" src="media/CaballeroOscuroCartel.jpg" alt="Cartel Caballero Oscuro"></a>
            </div>
          </div>
          <div class="car">
            <div class="col-4 col22">
              <a href="film.html"><img class="MetalJacket" src="media/FullMetalJacketCartel.jpg" alt="Cartel Chaqueta Metálica"></a>
            </div>
          </div>
          <div class="car">
            <div class="col-4 col33">
              <a href="film.html"><img class="PulpFiction" src="media/PulpFictionCartel.jpg" alt="Cartel Pulp Fictión"></a>
            </div>
          </div>
        </div>
        <div class="fila tres">
          <div class="car">
            <div class="col-4 col31">
              <a href="film.html"><img class="Padrino" src="media/TheGodfatherCartel.jpg" alt="Cartel El Padrino"></a>
            </div>
          </div>
          <div class="car">
            <div class="col-4 col32">
              <a href="film.html"><img class="CadenaPerpetua" src="media/CadenaPerpetuaCartel.jpg" alt="Cartel Cadena Perpetua"></a>
            </div>
          </div>
          <div class="car">
            <div class="col-4 col33">
              <a href="film.html"><img class="NaranjaMecanica" src="media/NaranjaMecanicaCartel.jpg" alt="Cartel Naranja Mecanica"></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- footer -->
  <div class="fila footer">
    <p class="copyright"><br><br>© Copyright by Javier Encinas y Víctor García-Bermejo. All Rights Reserved.</p>
  </div>
</body>
</html>
